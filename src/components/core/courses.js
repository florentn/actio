import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import Current from '../elements/current';
import Popular from '../elements/popular';
import designStyle from '../../resources/style/style';
import * as fakeData from '../../resources/data/data';

export default class Courses extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {
        
        return (
            <View style={styles.container}>
                <View style={styles.containerTop}>
                    <Current scenario={'courses'} data={fakeData.STRUCTURE['courses'].current} />
                </View>
                <View style={styles.containerBottom}>
                    <Popular data={ fakeData.STRUCTURE['courses'].catalog }/>
                </View>
            </View>
        );

    }

}

const styles = StyleSheet.create(designStyle.custom.courses);
