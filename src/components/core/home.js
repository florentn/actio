import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Current from '../elements/current';
import Agenda from '../elements/agenda';
import designStyle from '../../resources/style/style';
import * as fakeData from '../../resources/data/data';

export default class Home extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {
        
        return (
            <View style={styles.container}>
                <View style={styles.containerTop}>
                    <Current scenario={'home'} data={fakeData.STRUCTURE['home'].current} />
                </View>
                <View style={styles.containerBottom}>
                    <Agenda data={fakeData.STRUCTURE['home'].agenda}/>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create(designStyle.custom.home);
