import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView } from 'react-native'
import LabelLarge from '../shared/label_lg';
import designStyle from '../../resources/style/style';

export default class Agenda extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { data } = this.props;

        return (
            <View style={styles.container}>
                <Text style={styles.text}>Upcoming</Text>
            
                <ScrollView style={styles.containerList}>
                    {Object.keys(data).map((course,index)=>{
                        // console.log('AGENDA COURSE : ', index, ' => ', data[course]);
                        return (<LabelLarge key={index} styleObj={designStyle.custom.agenda.label} data={data[course]} />);
                    })}
                </ScrollView>             
            </View>
        );
    }

}

const styles = StyleSheet.create(designStyle.custom.agenda.style);
