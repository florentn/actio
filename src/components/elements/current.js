import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import Upcoming from '../shared/upcoming';
import Bubble from '../shared/bubble';
import CountdowComp from '../shared/countdown';
import DesignButton from '../shared/designButton';
import LabelSmall from '../shared/label_sm';
import designStyle from '../../resources/style/style';

export default class Current extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { bubble, button, countdown, upcoming } = this.props.data;
        // console.log('CURRENT DATA COUNTDOWN : ', countdown );
        // console.log('CURRENT STYLES UPCOMING : ', designStyle.custom.current.upcoming );

        return (
            <View style={styles.container }>
                <Upcoming styleObj={designStyle.custom.current.upcoming} data={upcoming} />
                { countdown && <CountdowComp styleObj={designStyle.custom.current.countdown} data={countdown} />}
                <Bubble styleObj={designStyle.custom.current.bubble} data={bubble} />
                { this.props.scenario === 'home' && <LabelSmall styleObj={designStyle.custom.current.label} data={button} /> } 
                { this.props.scenario !== 'home' && <DesignButton styleObj={designStyle.custom.current.button} data={button} /> } 
            </View>
        );
    }

}

const styles = StyleSheet.create(designStyle.custom.current.style);
