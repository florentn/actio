import React, { Component } from 'react'
import { StyleSheet, Image, Text, View } from 'react-native'
import designStyle from '../../resources/style/style';
import Carousel from 'react-native-snap-carousel';
import LabelXLarge from '../shared/label_xl';

export default class Popular extends Component {
    constructor(props){
        super(props);
        this.state = { activeIndex:0,  carouselItems: props.data };
    }
    
    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render () {
    
        return (
            <View style={styles.container}>
                <Carousel ref={(c) => { this._carousel = c; }} layout={designStyle.custom.popular.carousel.layout} 
                data={this.state.carouselItems} renderItem={this.renderItem}
                sliderWidth={designStyle.custom.popular.carousel.sliderWidth} itemWidth={designStyle.custom.popular.carousel.itemWidth} />
            </View>
        );

    }

    renderItem = ({item, index}) => {
        return (
            <View key={index} style={styles.slide}>
                <LabelXLarge styleObj={designStyle.custom.popular.label} data={item} />
            </View>
        );
    }
    
}

const styles = StyleSheet.create(designStyle.custom.popular.style);
