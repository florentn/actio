import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Home from './core/home';
import Courses from './core/courses';
import Calendar from './core/calendar';
// import Settings from './core/settings';
// import Icon from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import designStyle from '../resources/style/style';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();


export default class Layout extends Component {
    constructor(props) {
        super(props);
    
    }        

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        return (    
            <View style={styles.container}>
                <NavigationContainer>
                    <Tab.Navigator barStyle={designStyle.custom.layout.tabs.settings.bar} tabBarOptions={designStyle.custom.layout.tabs.settings.options} >
                        <Tab.Screen name={'home'} options={{tabBarLabel: '', tabBarIcon: () => (<Icon name={'home'} color={'white'} size={25} type='font-awesome' /> )}} children={()=> <Home /> } />
                        <Tab.Screen name={'courses'} options={{tabBarLabel: '', tabBarIcon: () => (<Icon name={'th-large'} color={'white'} size={25} type='font-awesome' /> )}} children={()=>  <Courses /> } />
                        <Tab.Screen name={'agenda'} options={{tabBarLabel: '', tabBarIcon: () => (<Icon name={'calendar'} color={'white'} size={25} type='font-awesome' /> )}} children={()=> <Calendar /> } />
                        <Tab.Screen name={'settings'} options={{tabBarLabel: '', tabBarIcon: () => (<Icon name={'bars'} color={'white'} size={25} type='font-awesome' /> )}} children={()=> <Text>DDD</Text> } />
                    </Tab.Navigator> 
                </NavigationContainer>
            </View>
        );
    }

}

const styles = StyleSheet.create(designStyle.custom.layout.style);
