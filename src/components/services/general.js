import Axios from 'axios';
import * as style from "../../resources/style/style";
import * as data from "../../resources/data/data";


//########################### GET SETTINGS + ARCHITECTURE ###################################
//########################### GET SETTINGS + ARCHITECTURE ###################################
export async function getStructure() {
    
    let results = {}
    results['styles'] = getStyle();
    results['data'] = getData();
    return results;

};


//########################### GET DATA ###################################
//########################### GET DATA ###################################
export async function getData() {
    
    return data.STRUCTURE;

};



//########################### GET STYLE ###################################
//########################### GET STYLE ###################################
export async function getStyle() {

    return style.custom;

};

