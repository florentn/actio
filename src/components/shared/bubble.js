import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';



export default class Bubble extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { styleObj, data } = this.props;
        return (
            <TouchableOpacity style={styleObj.container} onPress={ () =>{} }>
                <Icon name={styleObj.icon.icon} size={styleObj.icon.size} color={'white'} type='font-awesome' />
                {/* <Icon name={styleObj.icon.icon} size={styleObj.icon.size} color={() => <LinearGradient colors={['#37a3cd', '#8cb5e8', '#ab8cf3']} start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}} />} type='font-awesome' /> */}

            </TouchableOpacity>
        );
    }
}
                    