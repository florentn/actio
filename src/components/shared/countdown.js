import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Icon } from 'react-native-elements';
import CountDown from 'react-native-countdown-component';

export default class CountdownComp extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { styleObj, data } = this.props;

        // console.log('######### COUNTDOWN : ', data, ' => ', styleObj );

        return (
            <View style={styleObj.container}>
                    <View style={styleObj.containerIcon}>
                        { data.remaining > 0 &&  <Icon name={'clock-o'} size={styleObj.size} color={styleObj.icon.color} type='font-awesome' /> } 
                        { data.remaining === 0 &&  <Icon name={'signal'} size={styleObj.size} color={styleObj.color} type='font-awesome' /> } 
                    </View>
                    
                    <View style={styleObj.containerText}>
                        { data.remaining > 0 &&  
                            <View style={styleObj.containerText}>
                                <Text style={styleObj.text}>Starting In</Text>
                                <CountDown  
                                    until={data.remaining} onFinish={() => {}}
                                    size={styleObj.timer.size} 
                                    digitStyle={styleObj.timer.digitStyle} 
                                    digitTxtStyle={styleObj.timer.digitTxtStyle}
                                    timeToShow={['H', 'M']} timeLabels={{h: null, m: null}} 
                                    separatorStyle={{color: '#1CC625'}}
                                    showSeparator />
                            </View>
                        } 
                        { data.remaining === 0 && <Text style={styleObj.text}>Live</Text> }
                    </View>
            </View>
        //     <View style={{ position: 'absolute', flex: 1, top: '10%', left: '15%', flexDirection: 'row', justifyContent: 'space-between' }}>
        //     <View style={{ flex: 1, paddingLeft: 10, backgroundColor: 'red' }}>
        //       <Text style={{ fontSize: 20, fontWeight: '200' }}>LEFT_ELEM</Text>
        //     </View>
        //     <View style={{ flex: 1, paddingRight: 10, backgroundColor: 'green' }}>
        //       <Text style={{ textAlign:'center' }}>CENTER</Text>
        //     </View>
        //     <View
        //       style={{ flex: 1, paddingRight: 10, backgroundColor: 'yellow' }}>
        //     </View>
        //   </View>
        
            );
    }
}

