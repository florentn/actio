import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default class DesignButton extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { styleObj, data } = this.props;
        return (

            <TouchableOpacity style={styleObj.container} onPress={() =>{} }>
                <LinearGradient colors={['#e03973', '#ef4a84', '#e03973']} start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}} style={styles.linearGradient}>
                    <Text style={styleObj.content}>{ data.text }</Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        borderRadius: 30
    },
});