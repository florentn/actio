import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements';
import { format } from 'date-fns'

export default class LabelLarge extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { data, styleObj } = this.props;
        // console.log('LABEL LARGE : ', data, ' => ', styleObj);

        return (
            <TouchableOpacity style={styleObj.container}>
                <View style={styleObj.containerImage}>
                   <Image style={styleObj.image} source={{uri: data.coach.src}} />
                </View>
                
                <View style={styleObj.containerDetails}>
                    <Text style={styleObj.date}>{format(new Date(data.datetime), "iii, dd.mm.yyyy  hh:mm")}</Text>
                    <Text style={styleObj.title}>{data.name}</Text>
                    
                    <View style={styleObj.containerStatus}>
                        <View style={styleObj.containerStatusIcon}>
                            <Icon name={(data.status == 'scheduled' ? 'clock-o' : 'window-close') } size={ styleObj.iconStatus.size } color={ styleObj.iconStatus.color } type='font-awesome' />
                        </View>  
                        <Text style={styleObj.status}>{data.status}</Text>
                    </View>  
                </View>

                <View style={styleObj.containerAction}>
                    <Icon name={'chevron-right'} size={ styleObj.iconAction.size } color={ styleObj.iconAction.color } type='font-awesome' />
                </View>
                
            </TouchableOpacity>
        );
    }
}

