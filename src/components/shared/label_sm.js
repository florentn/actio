import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements';
import { format } from 'date-fns'

export default class LabelSmall extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { data, styleObj } = this.props;

        return (
            <TouchableOpacity style={styleObj.container}>
                <View style={styleObj.containerImage}>
                   <Image resizeMode={"cover"} style={styleObj.image} source={{uri: data.coach.src}} />
                </View>
                
                <View style={styleObj.containerDetails}>
                    <Text style={styleObj.date}>{format(new Date(data.datetime), "iii, dd.mm.yyyy  hh:mm")}</Text>
                    <View style={styleObj.containerCoach}>
                        <Text style={styleObj.coach1}>with </Text>
                        <Text style={styleObj.coach2}>{data.coach.name}</Text>
                    </View>
                </View>
                
                <View style={styleObj.containerAction}>
                    <Icon name={styleObj.iconAction.icon} size={styleObj.iconAction.size} color={styleObj.iconAction.color} type='font-awesome' />
                </View>
                
            </TouchableOpacity>
        );
    }
}

