import React, { Component } from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';

export default class Upcoming extends Component {
    constructor(props) {
        super(props) 
    }

    // ############################ RENDER ###################################
    // ############################ RENDER ###################################
    render() {

        const { data, styleObj } = this.props;
    
        return (
            <View style={styleObj.container }>
                <Image resizeMode={'cover'} style={styleObj.image} source={{uri: data.src}} />
                <View style={styleObj.containerText }>
                    <Text style={styleObj.name} >{data.name}</Text>
                </View>
            </View>
        );
    }
}
