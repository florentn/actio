export const STRUCTURE = {
    home:{
        current:{
            bubble:{ icon: 'user' },
            upcoming:{
                src: 'https://bnlschoolpictures.com/wp-content/uploads/2016/08/256-1080x1350.jpg',
                name: 'Morning Meditation'
            },
            countdown: { remaining: 1000 },
            button: { datetime: '2021-01-07T12:30:00', name: 'Morning Meditation', status: 'scheduled', coach : {name: 'Anna-Sophie', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png' }}
        },
        agenda:[
            { datetime: '2021-01-07T12:30:00', name: 'Strong Stretch', status: 'scheduled', coach : {name: 'Anna-Maria', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Soft Focus', status: 'scheduled', coach : {name: 'Katerina', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Care & Love', status: 'scheduled', coach : {name: 'Judith', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }
        ]
    },
    courses:{
        current:{
            bubble:{ icon: '2021-01-07T12:30:00' },
            upcoming:{
                src: 'https://bnlschoolpictures.com/wp-content/uploads/2016/08/256-1080x1350.jpg',
                name: 'Morning Meditation'
            },
            button: { text: 'Take a look' }
        },
        catalog:[
            { datetime: '2021-01-07T12:30:00', name: 'Morning Meditation', status: 'scheduled', coach : {name: 'Anna-Sophie', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Strong Stretch', status: 'scheduled', coach : {name: 'Anna-Maria', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Soft Focus', status: 'scheduled', coach : {name: 'Katerina', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Care & Love', status: 'scheduled', coach : {name: 'Judith', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }
        ]
    },
    calendar:{
        current:{
            bubble:{ icon: '2021-01-07T12:30:00' },
            upcoming:{
                src: 'https://bnlschoolpictures.com/wp-content/uploads/2016/08/256-1080x1350.jpg',
                name: 'Morning Meditation'
            },
            countdown: { remaining: 0 },
            button: { text: 'Join session' }
        },
        agenda:[
            { datetime: '2021-01-07T12:30:00', name: 'Strong Stretch', status: 'scheduled', coach : {name: 'Anna-Maria', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Soft Focus', status: 'scheduled', coach : {name: 'Katerina', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }, 
            { datetime: '2021-01-07T12:30:00', name: 'Care & Love', status: 'scheduled', coach : {name: 'Judith', src: 'https://uploads-ssl.webflow.com/5f5dfb3369a895a2fe88bbb1/5f6f67978d8b7167197136d2_A7308356_fav%402x.png'} }
        ]
    },
}