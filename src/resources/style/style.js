import colors from './subStyle/colors';
// import fonts from './subStyle/fonts';
import toasts from './subStyle/toasts';
import spinner from './subStyle/spinner';
import custom from './subStyle/custom';

export default{
    colors,
    toasts,
    spinner,
    custom,
    // fonts,
};

