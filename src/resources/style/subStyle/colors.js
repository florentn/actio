export default {
    primary: "#ad182f",
    primaryDisabled: "#D58B97",
    secondary: "#4693BD",
    secondaryDisabled: "#8BBBD5",
    black: "#000",
    white: "#fff",
    dark: "#0c0c0c",
    mediumDark: "#6e6969",
    mediumLight: "#d3d3d3",
    light: "#f8f4f4",
    danger: "#ff5252",
    gold: '#ffd700'
}