
export default {
    layout:{
        style:{ 
            container: { flex: 1, alignItems: 'stretch', backgroundColor: 'green' },
            logo: { position: 'absolute', top: 10, left: 20, zIndex: 2 },
            menu: { flex: 1, position: 'absolute', bottom: 85, zIndex: 9, borderRadius:10 },
            icon_container: { position: 'absolute', bottom: 30, right: 20, zIndex: 10, paddingHorizontal: 0, paddingVertical:10 },
            containerSplash: { backgroundColor: 'red', width: '100%', height: '100%', flexDirection:'column', justifyContent:'center', alignContent: 'center' },
        },
        tabs:{
            settings:{
                options:{
                    style:{ borderBottomRightRadius: 0, borderBottomLeftRadius: 0, backgroundColor: '#000000', paddingVertical: 15 },
                    activeTintColor: '#aaa123',
                    inactiveTintColor: '#ffffff',
                    labelStyle:{ paddingVertical:0, fontSize: 10, textTransform: 'uppercase' }
                },
                bar:{ marginLeft: 10, marginRight: 10, borderRadius: 10 }
            },
            structure:{
                home:{ 
                    // tab: { tabBarLabel: '', tabBarIcon: () => ( <Icon name={'home'} color={'white'} size={20} type='font-awesome' /> ) },
                    // children: <Home structure={this.state.structure}/>
                },
                courses:{ 
                    // tab: { tabBarLabel: '', tabBarIcon: () => ( <Icon name={'th-large'} color={'white'} size={20} type='font-awesome' /> )},
                    // children: <Courses structure={this.state.structure} />
                },
                agenda:{ 
                    // tab: { tabBarLabel: '', tabBarIcon: () => ( <Icon name={'calendar-day'} color={'white'} size={20} type='font-awesome' /> )},
                    // children: <Agenda structure={this.state.structure} />
                },
                settings:{ 
                    // tab: { tabBarLabel: '', tabBarIcon: () => ( <Icon name={'bars'} color={'white'} size={20} type='font-awesome' /> )},
                    // children: <Settings structure={this.state.structure} />
                }
}
        }
    },
    home:{
        container: { flex: 1 },
        containerTop: { flex: 7 },
        containerBottom: { flex: 4 }
    },
    courses:{
        container: { flex: 1},
        containerTop: { flex: 7},
        containerBottom: { flex: 4, backgroundColor: 'black'}
    },
    calendar:{
        container: { flex: 1},
        containerTop: { flex: 7},
        containerBottom: { flex: 4}
    },
    settings:{
        container: { flex: 1 },
    },
    current:{
        style: {container: { flex: 1 }},
        upcoming: { 
            container: { flex: 1 },
            image: { height: '110%' },
            containerText: { position: 'absolute', top: '50%', left: '10%', width: '30%' },
            name: { color: 'white', fontSize:25, fontFamily: 'arial',textShadowColor: 'rgba(0, 0, 0, 0.75)', textShadowOffset: {width: -1, height: 1}, textShadowRadius: 10 } 
        },
        bubble: {     
            container: { position: 'absolute', top: '7%', right: '7%', borderRadius: 75, backgroundColor: '#444444', paddingVertical: 7, paddingHorizontal: 10 },
            icon: { icon: 'user', color: 'white', size: 20} 
        },
        countdown: { 
            container: { position: 'absolute', top: '15%', left: '10%', paddingVertical:7, paddingHorizontal: 15, backgroundColor: 'rgba(0,0,0,0.2)', borderRadius: 40, alignItems: 'baseline' },
            containerIcon: { flex: 1, paddingVertical: 7 },
            containerText: { flex: 3, paddingHorizontal: 5 },
            text: { fontSize: 12, color: 'white', fontWeight: 'bold' },
            icon: { size: 20, color: 'white' },
            timer: { size: 15, digitStyle: {backgroundColor: 'rgba(0,0,0,0)'}, digitTxtStyle: {color: '#83f6a2'} }
        },
        button: { 
            container: {position: 'absolute', width:'70%', bottom: '10%', left:'15%', borderRadius: 40, elevation: 5 },
            content: {color: 'white', fontSize: 15, textAlign: 'center', paddingVertical: 12, textTransform:'uppercase' }
        },
        label: { 
            container: { flex: 1, flexDirection: 'row', position: 'absolute', width:'60%', height: 50, padding:7,  bottom: '10%', left:'10%', backgroundColor: 'rgba(10,10,10,0.85)', borderRadius: 40, },
            containerImage: { flex: 2, padding: 3},
            image: { borderRadius: 75, height: '100%', width: '100%' },
            containerDetails: { flex: 9, flexDirection: 'column', paddingHorizontal: 10 },
            containerAction: { flex: 2, justifyContent: 'center', alignContent: 'center', paddingHorizontal: 0 },
            date: { flex: 1, color: 'white', fontSize: 11 },
            containerCoach: { flex: 1, flexDirection: 'row', marginBottom: 5},
            coach1: { flex: 2, color: 'white', fontSize: 12 },
            coach2: { flex: 10, color: 'white', fontWeight: 'bold', fontSize: 12 },
            iconAction: { icon: 'chevron-right', size: 12, color: 'white' },
        }
    },
    agenda:{
        style: {
            container: { flex: 1, flexDirection: 'column', backgroundColor: 'black', padding:30 },
            text: { color: 'white', fontSize: 20, fontWeight: 'bold', paddingVertical: 10 },
            containerList: { flex: 8 }
        },
        label: { 
            container: { flex: 1, flexDirection: 'row', width:'100%', height: 70, padding:7, backgroundColor: 'rgba(10,10,10,0)', borderBottomWidth: 0.5, borderBottomColor:  '#888888' },
            containerImage: { flex: 2, padding: 3},
            image: { borderRadius: 5, height: 50, width: 50 },
            containerDetails: { flex: 9, flexDirection: 'column', paddingHorizontal: 10 },
            date: { color: 'white', fontSize: 11 },
            title: { fontSize: 15, fontWeight: 'bold', color: 'white' },
            containerStatus: { flexDirection: 'row', paddingVertical:3 },
            containerStatusIcon: { flex: 1, textAlign: 'left' },
            status: { flex: 8, fontSize:10, fontWeight: 'bold', color: 'white', textTransform:'uppercase' },
            containerAction: { flex: 2, justifyContent: 'center', alignContent: 'center', paddingHorizontal: 0 },
            iconAction: { size: 12, color: 'white' },
            iconStatus: { size: 12, color: 'white' },
        }
    },
    catalog:{
        container: { flex: 1, alignItems: 'stretch', backgroundColor: 'green' },
        carousel: { 
            container: { position: 'absolute', bottom: 30, right: 20, zIndex: 10, paddingHorizontal: 0, paddingVertical:10 },
            image: { backgroundColor: 'red', width: '100%', height: '100%', flexDirection:'column', justifyContent:'center', alignContent: 'center' },
            text: { backgroundColor: 'red', width: '100%', height: '100%', flexDirection:'column', justifyContent:'center', alignContent: 'center' }
        }
    },
    popular:{
        style:{
            container: { position: 'absolute', bottom: 30, right: 20, zIndex: 10, paddingHorizontal: 0, paddingVertical:10 },
            slide: { backgroundColor:'teal', borderRadius: 5, height: 250, width: 400, marginLeft: 10, marginRight: 10 },
        },
        carousel:{ sliderWidth: 400, itemWidth: 600, layout: 'default'},
        label: { 
            container: { flex: 1, width:'100%'},
            image: { flex: 1 },
            containerDetails: { position: 'absolute', bottom:'10%', right:'10%', flexDirection: 'column', paddingHorizontal: 10, backgroundColor: 'rgba(0,0,0,0.7)', padding:20, borderRadius: 20 },
            date: { color: 'white', fontSize: 11 },
            title: { fontSize: 15, fontWeight: 'bold', color: 'white' },
            containerStatus: { flexDirection: 'row', paddingVertical:3 },
            containerStatusIcon: { flex: 1, textAlign: 'left' },
            status: { flex: 8, fontSize:10, fontWeight: 'bold', color: 'white', textTransform:'uppercase' },
            containerAction: { flex: 2, justifyContent: 'center', alignContent: 'center', paddingHorizontal: 0 },
            iconAction: { size: 12, color: 'white' },
            iconStatus: { size: 12, color: 'white' },
        }
    }
    
}
