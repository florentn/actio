
export default {
    title: {
        fontFamily:'FuturaU',
        fontSize: 18,
        // fontWeigth: 700,
        textAlign:'left',
        paddingHorizontal:10
    },
    text: {
        fontFamily:'FuturaM',
        fontSize: 12,
        textAlign:'left',
        paddingHorizontal:10,
    },
    success: {
        width: '100%',
        backgroundColor:'white', 
        padding:20, 
        height:80 
    },
    info: {
        width: '100%',
        backgroundColor:'white', 
        padding:20
    },
    failure: {
        width: '100%',
        backgroundColor:'white', 
        padding:20
    }
}